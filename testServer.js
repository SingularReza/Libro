const express = require('express');
const path = require('path');
const fs = require('fs');

const app = express();
const folder = './Books';
var data = {};
data['author'] = [];

app.use('/', express.static(__dirname));
app.listen(8080, () => console.log('listening on port 8080'));

app.get('/index.html', function(req, res){
	res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/data', function(req, res){

	fs.readdir(folder, (err, files) => {
		files.forEach(file =>{
		console.log(file);
		data['author'].push(file);
		});
	});

	res.send(JSON.stringify(data));
});
